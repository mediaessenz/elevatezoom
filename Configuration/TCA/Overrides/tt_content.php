<?php

$GLOBALS['TCA']['tt_content']['palettes']['imagelinks'] = [
	'showitem' => '
        image_zoom;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:image_zoom_formlabel,
        elevate_zoom;Elevate Zoom'
];

$GLOBALS['TCA']['tt_content']['columns']['elevate_zoom'] = [
	'label' => 'Elevate Zoom',
	'exclude' => true,
	'config' => [
		'type' => 'check',
        'renderType' => 'checkboxToggle',
        'default' => false
	]
];
