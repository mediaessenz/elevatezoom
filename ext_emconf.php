<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "elevatezoom".
 *
 * Auto generated 28-10-2014 00:04
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Elevate Zoom Js',
    'description' => 'Plugin to include elevate zoom js.',
    'category' => 'frontend',
    'version' => '3.0.0',
    'state' => 'stable',
    'clearcacheonload' => 1,
    'author' => 'Alexander Grein',
    'author_email' => 'alexander.grein@gmail.com',
    'author_company' => 'MEDIA::ESSENZ',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-13.4.99',
            'bootstrap_package' => '13.0.0-15.99.99',
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
];
