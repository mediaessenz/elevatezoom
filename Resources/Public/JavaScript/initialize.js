/**
 * Created by alex on 02.01.16.
 */
(function($, window, document, undefined) {
  "use strict";
  // code using $ as alias to jQuery
  $(function() {
    // more code using $ as alias to jQuer
	  $('.elevatezoom').each(function() {
		  var $this = $(this);
		  $this.elevateZoom({
								 responsive: true,
								 loadingIcon: true,
								 scrollZoom : true,
								 zoomWindowPosition: 13,
								 zoomWindowFadeIn: 500,
								 zoomWindowFadeOut: 750,
								 zoomWindowWidth: $this.outerWidth(),
								 zoomWindowHeight: 300
							 });
	  });
  });
})(jQuery, window, document);